/*
example include file
*/

#ifndef HELLOMAKE_H
#define HELLOMAKE_H



#include "stm32f0xx_conf.h"
#include "stm32f0xx.h"
I2C_InitTypeDef         I2Clib_InitStructure;
GPIO_InitTypeDef        GPIOlib_InitStructure;
extern uint8_t I2C_write(I2C_TypeDef* I2Cx,uint8_t address,uint8_t* buffer,uint8_t num,long timeout,uint8_t state);
extern uint8_t I2C_read(I2C_TypeDef* I2Cx,uint8_t address,uint8_t* buffer,uint8_t num,long timeout,uint8_t state);
extern void I2C1_Config(void);
#define FAIL 0
#define SUCCCESS 1
#define END 1
#define NO_END 0

#endif /* SHAPE_H */
