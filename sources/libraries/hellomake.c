#include <hellomake.h>
uint8_t I2C_write(I2C_TypeDef* I2Cx,uint8_t address,uint8_t* buffer,uint8_t num,long timeout,uint8_t state){
    long timecount=timeout*10000;
    I2C_TransferHandling(I2Cx,address<<1,num,I2C_SoftEnd_Mode,I2C_Generate_Start_Write);
    while ((!I2C_GetFlagStatus(I2Cx,I2C_FLAG_TXIS))&&(timecount--));
    if(timecount==0) return FAIL;
    else{
    while(I2Cx->CR2&I2C_CR2_NBYTES){
         I2C_SendData(I2C1,*buffer++);
         while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_TXE));
    }
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_TC));
    I2C_GenerateSTOP(I2C1, state);
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_BUSY));
    return SUCCESS;
    }
    }
uint8_t I2C_read(I2C_TypeDef* I2Cx,uint8_t address,uint8_t* buffer,uint8_t num,long timeout,uint8_t state){
long timecount=timeout*10000;
  I2C_TransferHandling(I2C1,address<<1,num,I2C_SoftEnd_Mode,I2C_Generate_Start_Read);
	 while(I2Cx->CR2&I2C_CR2_NBYTES){
	 timecount=timeout*10000;
        while ((!I2C_GetFlagStatus(I2Cx,I2C_FLAG_RXNE))&&(timecount--));
        if(timecount==0) break;
         *buffer++=I2C_ReceiveData(I2C1);
         }
	if(timecount==0) {return FAIL;}
	else{
    I2C_GenerateSTOP(I2C1, state);
    while (!I2C_GetFlagStatus(I2Cx,I2C_FLAG_BUSY));
    return SUCCESS;
    }
    }
    void I2C1_Config(void){

    // cap clock cho ngoai vi va I2C
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);							// su dung kenh I2C2 cua STM32
    RCC_APB2PeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_4);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_4);
    // cau hinh chan SDA va SCL
    GPIOlib_InitStructure.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;						//PA9 - SCL, PA10 - SDA
    GPIOlib_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIOlib_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIOlib_InitStructure.GPIO_OType = GPIO_OType_OD;
    GPIOlib_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIOlib_InitStructure);

    // cau hinh I2C
    I2Clib_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
    I2Clib_InitStructure.I2C_DigitalFilter = 0x00;
    I2Clib_InitStructure.I2C_Mode = I2C_Mode_I2C;
    //I2C_InitStructure.I2C_DutyCycle = I2C_DutyCycle_2;
    I2Clib_InitStructure.I2C_OwnAddress1 = 0; //
    I2Clib_InitStructure.I2C_Ack = I2C_Ack_Disable;
    I2Clib_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
    I2Clib_InitStructure.I2C_Timing =0x10310309;//0x10310309
    I2C_Init(I2C1, &I2Clib_InitStructure);
    // cho phep bo I2C hoat dong
    I2C_Cmd(I2C1, ENABLE);

}
