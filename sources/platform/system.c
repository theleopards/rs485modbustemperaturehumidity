/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include "core_cm0.h"

#include "system.h"
#include "app.h"
#include "port.h"

/*****************************************************************************/
/* linker variable                                                           */
/*****************************************************************************/
extern uint32_t _ldata;
extern uint32_t _data;
extern uint32_t _edata;
extern uint32_t _bss;
extern uint32_t _ebss;
extern uint32_t _sstack;
extern uint32_t _estack;

extern void (*__preinit_array_start[])();
extern void (*__preinit_array_end[])();
extern void (*__init_array_start[])();
extern void (*__init_array_end[])();
extern void _init();

/*****************************************************************************/
/* static function prototype                                                 */
/*****************************************************************************/
/*****************************/
/* system interrupt function */
/*****************************/
void default_handler();
void reset_handler();

/*****************************/
/* user interrupt function   */
/*****************************/

/* cortex-M processor fault exceptions */
void nmi_handler()          __attribute__ ((weak));
void hard_fault_handler()   __attribute__ ((weak));
void mem_manage_handler()   __attribute__ ((weak));
void bus_fault_handler()    __attribute__ ((weak));
void usage_fault_handler()  __attribute__ ((weak));

/* cortex-M processor non-fault exceptions */
void dg_monitor_handler()   __attribute__ ((weak, alias("default_handler")));
void pendsv_handler()       __attribute__ ((weak, alias("default_handler")));
void svc_handler()          __attribute__ ((weak, alias("default_handler")));
void systick_handler();

/*****************************************************************************/
/* system variable                                                           */
/*****************************************************************************/

/*****************************************************************************/
/* interrupt vector table                                                    */
/*****************************************************************************/
__attribute__((section(".isr_vector")))
void (* const isr_vector[])() = {
		((void (*)())(uint32_t)&_estack)  , /* Top of Stack */
		reset_handler					  ,
		nmi_handler						  ,
		hard_fault_handler				  ,
		0								  ,
		0								  ,
		0								  ,
		0								  ,
		0								  ,
		0								  ,
		0								  ,
		svc_handler						  ,
		0								  ,
		0								  ,
		pendsv_handler					  ,
		systick_handler					  ,
		default_handler                   , /* Window WatchDog              */
		0                                 , /* Reserved                     */
		default_handler                   , /* RTC through the EXTI line    */
		default_handler                   , /* FLASH                        */
		default_handler                   , /* RCC                          */
		default_handler                   , /* EXTI Line 0 and 1            */
		default_handler                   , /* EXTI Line 2 and 3            */
		default_handler                   , /* EXTI Line 4 to 15            */
		0                                 , /* Reserved                     */
		default_handler                   , /* DMA1 Channel 1               */
		default_handler                   , /* DMA1 Channel 2 and Channel 3 */
		default_handler                   , /* DMA1 Channel 4 and Channel 5 */
		default_handler                   , /* ADC1                         */
		default_handler                   , /* TIM1 Break, Update, Trigger and Commutation */
		default_handler                   , /* TIM1 Capture Compare         */
		0                                 , /* Reserved                     */
		TIM3_IRQHandler                   , /* TIM3                         */
		0                                 , /* Reserved                     */
		0                                 , /* Reserved                     */
		default_handler                   , /* TIM14                        */
		0                                 , /* Reserved                     */
		default_handler                   , /* TIM16                        */
		default_handler                   , /* TIM17                        */
		default_handler                   , /* I2C1                         */
		0                                 , /* Reserved                     */
		default_handler                   , /* SPI1                         */
		0                                 , /* Reserved                     */
		USART1_IRQHandler                 , /* USART1                       */
		0                                 , /* Reserved                     */
		0                                 , /* Reserved                     */
		0                                 , /* Reserved                     */
		0                                 , /* Reserved                     */
		};

void _init() {
	/* dummy */
}

/*****************************************************************************/
/* static function defination                                                */
/*****************************************************************************/
void default_handler() {
	FATAL("SY", 0xEE);
}

void reset_handler() {
	volatile uint32_t *pSrc	= &_ldata;
	volatile uint32_t *pDest	= &_data;
	volatile unsigned i, cnt;

	/* init system */
	SystemInit();

	/* copy init data from FLASH to SRAM */
	while(pDest < &_edata) {
		*pDest++ = *pSrc++;
	}

	/* zero bss */
	for (pDest = &_bss; pDest < &_ebss; pDest++) {
		*pDest = 0UL;
	}

	/* invoke all static constructors */
	cnt = __preinit_array_end - __preinit_array_start;
	for (i = 0; i < cnt; i++)
		__preinit_array_start[i]();

	_init();

	cnt = __init_array_end - __init_array_start;
	for (i = 0; i < cnt; i++)
		__init_array_start[i]();

	SystemCoreClockUpdate();

	/* entry app function */
	main_app();
}

/****************************************
 * system memory function.              *
 ****************************************/

/***************************************/
/* cortex-M processor fault exceptions */
/***************************************/
void nmi_handler() {
	FATAL("SY", 0x01);
}

void hard_fault_handler() {
	FATAL("SY", 0x02);
}

void mem_manage_handler() {
	FATAL("SY", 0x03);
}

void bus_fault_handler() {
	FATAL("SY", 0x04);
}

void usage_fault_handler() {
	FATAL("SY", 0x05);
}

/*******************************************/
/* cortex-M processor non-fault exceptions */
/*******************************************/
void systick_handler() {
	FATAL("SY", 0x06);
}

//-----------------------------------------------------------------------------
void DelayMicroSeconds(u32t nbrOfUs) {
	u32t i;
	for(i = 0; i < nbrOfUs; i++)
	{
		__NOP();  // nop's may be added or removed for timing adjustment
		__NOP();
		__NOP();
		__NOP();
	}
}
