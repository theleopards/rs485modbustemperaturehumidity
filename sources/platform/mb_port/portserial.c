/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

#include "port.h"
#include "platform.h"
#include "stm32f0xx.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- static functions ---------------------------------*/
static void prvvUARTTxReadyISR( void );
static void prvvUARTRxISR( void );

/* ----------------------- Start implementation -----------------------------*/
void
vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
	/* If xRXEnable enable serial receive interrupts. If xTxENable enable
	 * transmitter empty interrupts.
	 */
	if ( xRxEnable ) {
		USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);
	}
	else {
		USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
		GPIO_SetBits(GPIOA, GPIO_Pin_4);
	}

	if ( xTxEnable ) {
		GPIO_SetBits(GPIOA, GPIO_Pin_4);
		USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	}
	else {
		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);
	}
}

BOOL
xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity )
{
	BOOL xResult = TRUE;

	USART_InitTypeDef       USART_InitStructure;
	GPIO_InitTypeDef        GPIO_InitStructure;

	(void)ucPORT;

	switch ( eParity ) {
	case MB_PAR_EVEN:
		USART_InitStructure.USART_Parity = USART_Parity_Even;
		break;
	case MB_PAR_ODD:
		USART_InitStructure.USART_Parity = USART_Parity_Odd;
		break;
	case MB_PAR_NONE:
		USART_InitStructure.USART_Parity = USART_Parity_No;
		break;
	}

	switch ( ucDataBits ) {
	case 7:
		if ( eParity == MB_PAR_NONE ) {
			/* not supported by our hardware. */
			xResult = FALSE;
		}
		else {
			USART_InitStructure.USART_WordLength = USART_WordLength_7b;
		}
		break;

	case 8:
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		break;

	case 9:
		if ( eParity == MB_PAR_NONE ) {
			/* not supported by our hardware. */
			xResult = FALSE;
		}
		else {
			USART_InitStructure.USART_WordLength = USART_WordLength_9b;
		}
		break;

	default:
		xResult = FALSE;
	}

	if (xResult) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);


		/* RS485 Direction */
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
		GPIO_ResetBits(GPIOA, GPIO_Pin_4);

		/* Configure USART Tx && Rx as alternate function push-pull */
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
		USART_InitStructure.USART_BaudRate = ulBaudRate;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(USART1, &USART_InitStructure);

		USART_Cmd(USART1, ENABLE);

		vMBPortSerialEnable(TRUE, TRUE);

		NVIC_SetPriority(USART1_IRQn, 0);
		NVIC_EnableIRQ(USART1_IRQn);
	}

	return xResult;
}

BOOL
xMBPortSerialPutByte( CHAR ucByte )
{
	USART_SendData(USART1, (uint8_t) ucByte);
	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
	return TRUE;
}

BOOL
xMBPortSerialGetByte( CHAR * pucByte )
{
	*pucByte = USART_ReceiveData(USART1);
	return FALSE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call
 * xMBPortSerialPutByte( ) to send the character.
 */
static void prvvUARTTxReadyISR( void )
{
	pxMBFrameCBTransmitterEmpty(  );
}

/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
static void prvvUARTRxISR( void )
{
	pxMBFrameCBByteReceived(  );
}

void USART1_IRQHandler( void )
{
	if ( USART_GetITStatus(USART1, USART_IT_TXE) != RESET )
	{
		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
		prvvUARTTxReadyISR();
	}

	if ( USART_GetITStatus(USART1, USART_IT_RXNE) != RESET )
	{
		prvvUARTRxISR();
	}
}
