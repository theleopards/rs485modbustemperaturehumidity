#ifndef __IO_CFG_H__
#define __IO_CFG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "port.h"
#include "platform.h"
#include "stm32f0xx.h"

#define SS1_IO_PIN				(GPIO_Pin_1)
#define SS1_IO_PORT				(GPIOB)
#define SS1_IO_CLOCK			(RCC_AHBPeriph_GPIOB)

#define SS2_IO_PIN				(GPIO_Pin_7)
#define SS2_IO_PORT				(GPIOA)
#define SS2_IO_CLOCK			(RCC_AHBPeriph_GPIOA)

#define SS3_IO_PIN				(GPIO_Pin_6)
#define SS3_IO_PORT				(GPIOA)
#define SS3_IO_CLOCK			(RCC_AHBPeriph_GPIOA)

#define SS4_IO_PIN				(GPIO_Pin_5)
#define SS4_IO_PORT				(GPIOA)
#define SS4_IO_CLOCK			(RCC_AHBPeriph_GPIOA)

typedef union {
	struct {
		uint8_t bit1		: 1;
		uint8_t bit2		: 1;
		uint8_t bit3		: 1;
		uint8_t bit4		: 1;
		uint8_t reserved	: 4;
	} set;
	uint8_t val;
} io_dip_switch_u;

extern void io_dip_switch_init();
extern uint16_t io_dip_switch_get();

extern void uart1_init(uint32_t baud);
extern void uart1_putc(uint8_t c);

#ifdef __cplusplus
}
#endif

#endif //__IO_CFG_H__
