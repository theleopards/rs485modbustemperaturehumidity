/**
 ******************************************************************************
 * @author: ThanNT
 * @date:   05/09/2016
 ******************************************************************************
**/
#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "app.h"
#include "platform.h"
#include "typedefs.h"

#define FATAL(s, c) \
	do { \
		DISABLE_INTERRUPTS(); \
		(void)s; \
		(void)c; \
		app_dbg_fatal((const int8_t*)s, (uint8_t)c); \
} while (0)

//=============================================================================
void DelayMicroSeconds(u32t nbrOfUs);
//=============================================================================
// Wait function for small delays.
//-----------------------------------------------------------------------------
// input:  nbrOfUs   wait x times approx. one micro second (fcpu = 8MHz)
// return: -
// remark: smallest delay is approx. 15us due to function call

#ifdef __cplusplus
}
#endif

#endif //__SYSTEM_H__
