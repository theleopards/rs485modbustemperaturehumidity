include sources/platform/mb_port/Makefile.mk

LDFILE = sources/platform/ak.ld

CFLAGS += -I./sources/platform/
CFLAGS += -I./sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/CMSIS/Device/ST/STM32F0xx/Include
CFLAGS += -I./sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/CMSIS/Include
CFLAGS += -I./sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/inc
CFLAGS += -I./sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/CMSIS/Device/ST/STM32F0xx/Include

VPATH += sources/platform
VPATH += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src

# C source files
SOURCES += sources/platform/system_stm32f0xx.c
SOURCES += sources/platform/platform.c
SOURCES += sources/platform/system.c
SOURCES += sources/platform/io_cfg.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_rcc.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_gpio.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_usart.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_misc.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_tim.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_i2c.c
SOURCES += sources/platform/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_flash.c

# C++ source files
SOURCES_CPP += sources/platform/mini_cpp.cpp

# ASM source files
SOURCES_ASM += sources/platform/sys_ctrl.s
