#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include "core_cm0.h"

#include "system.h"
#include "io_cfg.h"

void io_dip_switch_init() {
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_AHBPeriphClockCmd(    SS1_IO_CLOCK  \
							  | SS2_IO_CLOCK \
							  | SS3_IO_CLOCK \
							  | SS4_IO_CLOCK \
							  , ENABLE);

	/* SS1 */
	GPIO_InitStructure.GPIO_Pin = SS1_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SS1_IO_PORT, &GPIO_InitStructure);

	/* SS2 */
	GPIO_InitStructure.GPIO_Pin = SS2_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SS2_IO_PORT, &GPIO_InitStructure);

	/* SS3 */
	GPIO_InitStructure.GPIO_Pin = SS3_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SS3_IO_PORT, &GPIO_InitStructure);

	/* SS4 */
	GPIO_InitStructure.GPIO_Pin = SS4_IO_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(SS4_IO_PORT, &GPIO_InitStructure);
}

uint16_t io_dip_switch_get() {
#define ST_COUNTER_FILTER_MIL		( 10 ) // Must be bigger 1.

	uint32_t st_counter_filter		= 0;
	uint32_t st_counter_timeout		= 100;
	io_dip_switch_u st_shadow_val;
	uint8_t st_tmp_val;

	while (1) {
		/* Check timeout */
		if (st_counter_timeout-- == 0) {
			return 0;
		}

		/* Get SW raw value */
		st_shadow_val.set.reserved = 0;
		st_shadow_val.set.bit1 = GPIO_ReadInputDataBit(SS1_IO_PORT, SS1_IO_PIN);
		st_shadow_val.set.bit2 = GPIO_ReadInputDataBit(SS2_IO_PORT, SS2_IO_PIN);
		st_shadow_val.set.bit3 = GPIO_ReadInputDataBit(SS3_IO_PORT, SS3_IO_PIN);
		st_shadow_val.set.bit4 = GPIO_ReadInputDataBit(SS4_IO_PORT, SS4_IO_PIN);

		/* First Check SW Val */
		if (st_counter_filter == 0) {
			st_tmp_val = st_shadow_val.val;
			st_counter_filter++;
		}
		else {
			if (st_tmp_val == st_shadow_val.val) {
				/* Read SW Value OK */
				if (st_counter_filter++ >= ST_COUNTER_FILTER_MIL) {
					return st_tmp_val;
				}
			}
			else {
				st_counter_filter = 0;
			}
		}
	}

	return 0;
}

void uart1_init(uint32_t baud) {
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	USART_Cmd(USART1, ENABLE);
}

void uart1_putc(uint8_t c) {
	USART_SendData(USART1, (uint8_t) c);
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET) {}
}

