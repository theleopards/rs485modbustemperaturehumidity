#include <string.h>

#include "app.h"
#include "app_data.h"
#include "platform.h"

#include "io_cfg.h"
#include "xprintf.h"

#include "mb.h"
#include "mbport.h"
#include "port.h"
#include "math.h"

#define REG_HOLDING_START		1
#define REG_HOLDING_NREGS		200

/* Note: Reference Links */
//https://www.mouser.com/datasheet/2/682/Sensirion_Humidity_Sensors_SHT3x_Datasheet_digital-971521.pdf

/* ----------------------- Static variables ---------------------------------*/
static USHORT usRegHoldingStart = REG_HOLDING_START;
static USHORT usRegHoldingBuf[REG_HOLDING_NREGS];

static hw_modbus_config_t hw_modbus_config_shadow;

void main_app() {
	etError   error;
	ft temperature;
	ft humidity;

	/* Init IO of DIP Switch */
	io_dip_switch_init();

	/* Get modbus config */
	internal_flash_read_firstpage((uint8_t*)&hw_modbus_config_shadow, sizeof(hw_modbus_config_t));

	/* Check first time of system initial */
	if (hw_modbus_config_shadow.Mode != MB_RTU) {
		memcpy(&hw_modbus_config_shadow, &hw_modbus_config_default, sizeof(hw_modbus_config_t));
		internal_flash_write_firstpage((uint8_t*)&hw_modbus_config_shadow, sizeof(hw_modbus_config_t));
	}

	/* Check modbus address config */
	if (hw_modbus_config_shadow.IsSwitchAddress) {
		/* ToDo: update switch address */
		hw_modbus_config_shadow.SlaveAddress = io_dip_switch_get();
	}

	eMBInit( hw_modbus_config_shadow.Mode, \
			 hw_modbus_config_shadow.SlaveAddress, \
			 hw_modbus_config_shadow.Port, \
			 baudrate_option[hw_modbus_config_shadow.BaudRate], \
			hw_modbus_config_shadow.Parity);

	/* Enable the Modbus Protocol Stack. */
	eMBEnable();

	/* SHT3x sensor init */
	SHT3X_Init(0x44);

	/* wait 50ms after power on */
	DelayMicroSeconds(50000);

	error = SHT3X_StartPeriodicMeasurment(REPEATAB_HIGH,FREQUENCY_2HZ);
	if (error != NO_ERROR) { } // do error handling here

	for ( ;; ) {
		/* Check modbus address config */
		if (hw_modbus_config_shadow.IsSwitchAddress) {
			/* ToDo: update switch address */
			hw_modbus_config_shadow.SlaveAddress = io_dip_switch_get();
		}

		/* Read sensor data */
		error = SHT3X_ReadMeasurementBuffer(&temperature, &humidity);
		if (error != NO_ERROR) { } // do error handling here

		/* Modbus polling */
		eMBPoll();

		/* Update register value */
		usRegHoldingBuf[0] = 10 * (temperature);
		usRegHoldingBuf[1] = 10 * (humidity);
	}
}

eMBErrorCode eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, eMBRegisterMode eMode ) {
	eMBErrorCode    eStatus = MB_ENOERR;
	int             iRegIndex;

	if( ( usAddress >= REG_HOLDING_START ) && ( usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS ) ) {
		iRegIndex = ( int )( usAddress - usRegHoldingStart );
		switch ( eMode ) {
		/* Pass current register values to the protocol stack. */
		case MB_REG_READ: {
			while( usNRegs > 0 )
			{
				*pucRegBuffer++ = ( UCHAR ) ( usRegHoldingBuf[iRegIndex] >> 8 );
				*pucRegBuffer++ = ( UCHAR ) ( usRegHoldingBuf[iRegIndex] & 0xFF );
				iRegIndex++;
				usNRegs--;
			}

			usRegHoldingBuf[100] = hw_modbus_config_shadow.SlaveAddress;
			usRegHoldingBuf[101] = hw_modbus_config_shadow.BaudRate;
		}
			break;

			/* Update current register values with new values from the
			 * protocol stack. */
		case MB_REG_WRITE: {
			BOOL is_update_flash = 0;

			while( usNRegs > 0 ) {
				usRegHoldingBuf[iRegIndex] = *pucRegBuffer++ << 8;
				usRegHoldingBuf[iRegIndex] |= *pucRegBuffer++;
				iRegIndex++;
				usNRegs--;
			}

			/* Update current register values with address values from the
			 * protocol stack. */

			/* Baudrate Setting */
			if (usRegHoldingBuf[101] >= BAUDRATE_OPTION_SIZE) {
				usRegHoldingBuf[101] = 3; /* 9600 */
			}

			if (hw_modbus_config_shadow.BaudRate != usRegHoldingBuf[101]) {
				hw_modbus_config_shadow.BaudRate = usRegHoldingBuf[101];
				is_update_flash = 1;
			}

			if (hw_modbus_config_shadow.SlaveAddress != usRegHoldingBuf[100] \
					&& hw_modbus_config_shadow.SlaveAddress > 1) {

				hw_modbus_config_shadow.SlaveAddress = usRegHoldingBuf[100];
				is_update_flash = 1;
			}

			if (is_update_flash) {

				internal_flash_write_firstpage((uint8_t*)&hw_modbus_config_shadow, sizeof(hw_modbus_config_t));

				eStatus = eMBInit( hw_modbus_config_shadow.Mode, \
								   hw_modbus_config_shadow.SlaveAddress, \
								   hw_modbus_config_shadow.Port, \
								   baudrate_option[hw_modbus_config_shadow.BaudRate], \
						hw_modbus_config_shadow.Parity);

				eStatus = eMBEnable();
			}
		}
			break;

		default:
			break;
		}
	}
	else {
		eStatus = MB_ENOREG;
	}

	return eStatus;
}

eMBErrorCode eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs ) {
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNRegs;
	return MB_ENOREG;
}

eMBErrorCode eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, eMBRegisterMode eMode ) {
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNCoils;
	(void)eMode;
	return MB_ENOREG;
}

eMBErrorCode eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete ) {
	(void)pucRegBuffer;
	(void)usAddress;
	(void)usNDiscrete;
	return MB_ENOREG;
}

/* ----------------------- Static Function ---------------------------------*/
void app_dbg_fatal( const int8_t* s, uint8_t c ) {
	int delayVal = 50;
	(void)s;
	(void)c;
	while (1) {
		vMBPortTimersDelay(delayVal);
	}
}
