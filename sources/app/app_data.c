#include <stdint.h>
#include <string.h>

#include"app_data.h"

uint32_t baudrate_option[BAUDRATE_OPTION_SIZE] = { \
	1200,	/* 0 */ \
	2400,	/* 1 */ \
	4800,	/* 2 */ \
	9600,	/* 3 */ \
	14400,	/* 4 */ \
	19200,	/* 5 */ \
	38400,	/* 6 */ \
	56000,	/* 7 */ \
	57600,	/* 8 */ \
	115200	/* 9 */ \
};

hw_modbus_config_t hw_modbus_config_default = {
	.Mode				=	MB_RTU,
	.IsSwitchAddress	=	1,
	.SlaveAddress		=	1,
	.Port				=	0,
	.BaudRate			=	3, /* 9600 */
	.Parity				=	MB_PAR_NONE
};

uint8_t internal_flash_write_firstpage(uint8_t* data, uint32_t len) {
	uint32_t page_program_shadow_data;

	if (len > FLASH_PAGE_SIZE) {
		return 0;
	}

	/* Unlock the Flash to enable the flash control register access */
	FLASH_Unlock();

	/* Clear pending flags (if any) */
	FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPERR);

	/* Erase page */
	if (FLASH_ErasePage(FLASH_USER_START_ADDR)!= FLASH_COMPLETE) {
		return 0;
	}

	for (uint32_t i = 0; i < len; i = i + 4) {
		page_program_shadow_data = 0;

		if ( (len - i) < 4) {
			for (uint32_t j = 0; j < (len - i); j++) {
				((uint8_t*)page_program_shadow_data)[j] = data[i];
			}
		}
		else {
			page_program_shadow_data = *((uint32_t*)data);
		}

		FLASH_ProgramWord(FLASH_PAGE_SIZE + i, page_program_shadow_data);
	}

	FLASH_Lock();

	return 1;
}

void internal_flash_read_firstpage(uint8_t* data, uint32_t len) {
	memcpy(data, (uint8_t*)FLASH_USER_START_ADDR, len);
}
