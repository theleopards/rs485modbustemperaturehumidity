#ifndef __APP_H__
#define __APP_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include "sht3x.h"
#include "system.h"

extern void main_app();
extern void app_dbg_fatal(const int8_t* s, uint8_t c);

#ifdef __cplusplus
}
#endif

#endif //__APP_H__
